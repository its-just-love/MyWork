package com.example.mywork;

import androidx.appcompat.app.AppCompatActivity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Fragment wechatFragment=new wechatfragment();
    private Fragment contactsFragment=new contactsfragment();
    private Fragment findFragment=new findfragment();
    private Fragment meFragment=new mefragment();

    private FragmentManager fragmentManager;

    private LinearLayout LinearLayout1,LinearLayout2,LinearLayout3,LinearLayout4;
    private ImageView ImageView1,ImageView2,ImageView3,ImageView4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        LinearLayout1=findViewById(R.id.LinearLayout_wechat);
        LinearLayout2=findViewById(R.id.LinearLayout_contacts);
        LinearLayout3=findViewById(R.id.LinearLayout_find);
        LinearLayout4=findViewById(R.id.LinearLayout_me);

        ImageView1=findViewById(R.id.wechat);
        ImageView2=findViewById(R.id.contacts);
        ImageView3=findViewById(R.id.find);
        ImageView4=findViewById(R.id.me);

        LinearLayout1.setOnClickListener(this);
        LinearLayout2.setOnClickListener(this);
        LinearLayout3.setOnClickListener(this);
        LinearLayout4.setOnClickListener(this);

        initFragment();
        showFragment(1);

    }

    private void initFragment(){              //初始化
        fragmentManager=getFragmentManager();
        FragmentTransaction transaction=fragmentManager.beginTransaction();
        transaction.add(R.id.id_content,wechatFragment);
        transaction.add(R.id.id_content,contactsFragment);
        transaction.add(R.id.id_content,findFragment);
        transaction.add(R.id.id_content,meFragment);
        transaction.commit();
    }

    private void hideFragment(FragmentTransaction transaction){
        transaction.hide(wechatFragment);
        transaction.hide(contactsFragment);
        transaction.hide(findFragment);
        transaction.hide(meFragment);
    }

    private void recover(){
        ImageView1.setImageResource(R.drawable.wechat1);
        ImageView2.setImageResource(R.drawable.contacts1);
        ImageView3.setImageResource(R.drawable.find1);
        ImageView4.setImageResource(R.drawable.me1);
    }

    @Override
    public void onClick(View view) {
        recover();
       switch(view.getId()){
           case R.id.LinearLayout_wechat:
               showFragment(1);
               break;
           case R.id.LinearLayout_contacts:
               showFragment(2);
               break;
           case R.id.LinearLayout_find:
               showFragment(3);
               break;
           case R.id.LinearLayout_me:
               showFragment(4);
               break;
           default:
               break;
       }
    }

    private void showFragment(int i) {
        FragmentTransaction transaction=fragmentManager.beginTransaction();
        hideFragment(transaction);
        switch(i){
            case 1:
                transaction.show(wechatFragment);
                ImageView1.setImageResource(R.drawable.wechat);
                break;
            case 2:
                transaction.show(contactsFragment);
                ImageView2.setImageResource(R.drawable.contacts);
                break;
            case 3:
                transaction.show(findFragment);
                ImageView3.setImageResource(R.drawable.find);
                break;
            case 4:
                transaction.show(meFragment);
                ImageView4.setImageResource(R.drawable.me);
                break;
            default:break;
        }
        transaction.commit();
    }

}